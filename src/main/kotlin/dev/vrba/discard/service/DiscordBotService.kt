package dev.vrba.discard.service

import dev.kord.common.annotation.KordPreview
import dev.kord.core.Kord
import dev.kord.core.event.gateway.ReadyEvent
import dev.kord.core.on
import dev.vrba.discard.configuration.DiscordConfiguration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Service

@Service
class DiscordBotService(private val configuration: DiscordConfiguration) : CommandLineRunner {

    @OptIn(KordPreview::class)
    override fun run(vararg args: String) {
        runBlocking(Dispatchers.IO) {
            val client = Kord(configuration.token)

            // TODO: Register other event listeners

            client.on<ReadyEvent> {
                client.editPresence { playing("cards against humanity") }
                client.createGlobalApplicationCommand("game", "Start a new game in this channel") {
                    int("win-points", "Number of points required to win the game") {
                        required = false
                    }
                }
            }

            client.login()
        }
    }
}