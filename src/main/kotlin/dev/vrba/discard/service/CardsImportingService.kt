package dev.vrba.discard.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.vrba.discard.entity.BlackCard
import dev.vrba.discard.entity.WhiteCard
import dev.vrba.discard.repository.BlackCardRepository
import dev.vrba.discard.repository.WhiteCardRepository
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class CardsImportingService(
    private val whiteCardRepository: WhiteCardRepository,
    private val blackCardRepository: BlackCardRepository,
    private val mapper: ObjectMapper
) : CommandLineRunner {

    private val logger = LoggerFactory.getLogger(this::class.qualifiedName)

    data class BlackCardSource(val text: String, val pick: Int)
    data class CardsAgainstJsonSource(val white: List<String>, val black: List<BlackCardSource>, val metadata: Any)

    override fun run(vararg args: String) {
        // The cards were already imported
        if (whiteCardRepository.count() > 0 && blackCardRepository.count() > 0) {
            return
        }

        val source = this::class.java.getResourceAsStream("/cards.json") ?: throw IllegalStateException("Cannot find the cards.json source file!")
        val cards = mapper.readValue<CardsAgainstJsonSource>(source)

        // Remove all previously imported cards
        whiteCardRepository.deleteAll()
        blackCardRepository.deleteAll()

        // Remove funky stuff from the CAH json file
        fun String.normalize(): String = this
            .replace("\\n", "\n") // Replace incorrectly encoded newlines
            .replace("\\.+$".toRegex(), "") // Remove trailing spaces

        val limit = 100 // characters
        val whiteCards = cards.white.filter { it.length <= limit }.map { WhiteCard(text = it.normalize()) }
        val blackCards = cards.black.filter { it.text.length <= limit }.map { BlackCard(text = it.text.normalize(), blanks = it.pick) }

        whiteCardRepository.saveAll(whiteCards)
        blackCardRepository.saveAll(blackCards)

        logger.info("Imported ${whiteCards.size} white and ${blackCards.size} black cards from the json source file")
    }
}