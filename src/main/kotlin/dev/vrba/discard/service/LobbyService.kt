package dev.vrba.discard.service

import dev.vrba.discard.entity.Lobby
import dev.vrba.discard.repository.LobbiesRepository
import org.springframework.stereotype.Service

@Service
class LobbyService(private val repository: LobbiesRepository) {

    fun createLobby(guild: Long, channel: Long, user: Long): Lobby {
        val message = createLobbyPlaceholderMessage(guild, channel)
        val lobby = repository.save(
            Lobby(
                owner = user,
                guild = guild,
                channel = channel,
                message = message
            )
        )

        updateLobbyEmbed(lobby)

        return lobby
    }

    private fun createLobbyPlaceholderMessage(guild: Long, channel: Long): Long {
        // Send a message to the passed text channel and return its id
        return 0
    }

    private fun updateLobbyEmbed(lobby: Lobby, /* message: Message? = null */) {
        // Update lobby embed based on the passed object
        return
    }
}