package dev.vrba.discard.repository

import dev.vrba.discard.entity.WhiteCard
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WhiteCardRepository : CrudRepository<WhiteCard, Int>