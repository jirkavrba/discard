package dev.vrba.discard.repository

import dev.vrba.discard.entity.Lobby
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface LobbiesRepository : CrudRepository<Lobby, Int>