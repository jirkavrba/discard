package dev.vrba.discard.repository

import dev.vrba.discard.entity.BlackCard
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BlackCardRepository : CrudRepository<BlackCard, Int>