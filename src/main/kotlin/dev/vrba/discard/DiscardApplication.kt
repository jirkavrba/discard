package dev.vrba.discard

import dev.vrba.discard.configuration.DiscordConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(DiscordConfiguration::class)
class DiscardApplication

fun main(args: Array<String>) {
	runApplication<DiscardApplication>(*args)
}
