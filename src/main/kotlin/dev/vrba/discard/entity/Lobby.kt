package dev.vrba.discard.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("lobbies")
data class Lobby(
    @Id
    val id: Int = 0,

    @Column("owner_id")
    val owner: Long,

    @Column("guild_id")
    val guild: Long,

    @Column("channel_id")
    val channel: Long,

    @Column("message_id")
    val message: Long,

    @Column("joined_players")
    val players: Set<Long> = emptySet()
)