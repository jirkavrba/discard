package dev.vrba.discard.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("white_cards")
data class WhiteCard(
    @Id
    val id: Int = 0,

    @Column("text")
    val text: String,
)